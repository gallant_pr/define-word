package com.bigscreen.defineword.home

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.OnLifecycleEvent
import android.widget.Toast
import com.bigscreen.defineword.DWApplication
import com.bigscreen.defineword.arch.UserAction
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_home.viewInputResult
import javax.inject.Inject

class HomeActivityDelegate(private val activity: HomeActivity) : HomeContract.View {

    @Inject
    lateinit var presenter: HomeContract.Presenter

    init {
        (activity.application as DWApplication).appComponent.plus(HomeModule()).inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        presenter.attach(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        presenter.detach()
    }

    override fun actions(): Observable<UserAction> = activity.viewInputResult.actions()

    override fun showData(model: HomeContract.ViewModel): Observable<out UserAction> {
        return when (model) {
            is HomeContract.ViewModel.HomeViewState -> activity.viewInputResult.bind(model)
        }
    }

    override fun showError(error: Throwable): Observable<out UserAction> {
        return activity.viewInputResult.bindError()
    }

}