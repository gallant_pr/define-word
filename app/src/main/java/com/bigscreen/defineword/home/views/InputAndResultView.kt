package com.bigscreen.defineword.home.views

import android.content.Context
import android.support.constraint.ConstraintLayout
import android.util.AttributeSet
import android.view.View
import com.bigscreen.defineword.R
import com.bigscreen.defineword.arch.UserAction
import com.bigscreen.defineword.ext.getString
import com.bigscreen.defineword.ext.rxClick
import com.bigscreen.defineword.home.HomeContract
import com.bigscreen.defineword.home.HomeUserAction
import io.reactivex.Observable
import kotlinx.android.synthetic.main.layout_input_n_result.view.buttonSearch
import kotlinx.android.synthetic.main.layout_input_n_result.view.inputWord
import kotlinx.android.synthetic.main.layout_input_n_result.view.textWordCategory
import kotlinx.android.synthetic.main.layout_input_n_result.view.textWordDefinition

class InputAndResultView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    init {
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        View.inflate(context, R.layout.layout_input_n_result, this)
    }

    fun actions(): Observable<UserAction> {
        return buttonSearch.rxClick().map {
            HomeUserAction.ActionSearch(inputWord.getString())
        }
    }

    fun bind(model: HomeContract.ViewModel.HomeViewState): Observable<UserAction> {
        with(model) {
            textWordCategory.text = wordCategory
            textWordDefinition.text = wordDefinition
        }
        return actions()
    }

    fun bindError(): Observable<UserAction> {
        textWordCategory.text = context.getString(R.string.error_title)
        textWordDefinition.text = context.getString(R.string.error_message)
        return actions()
    }
}