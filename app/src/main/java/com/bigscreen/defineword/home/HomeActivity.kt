package com.bigscreen.defineword.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bigscreen.defineword.R

class HomeActivity : AppCompatActivity(), HomeContract.DelegatingView {

    override val activityDelegate by lazy { HomeActivityDelegate(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_home)

        lifecycle.addObserver(activityDelegate)
    }

    override fun onDestroy() {
        super.onDestroy()
        lifecycle.removeObserver(activityDelegate)
    }

}