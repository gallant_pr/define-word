package com.bigscreen.defineword.home

import com.bigscreen.defineword.arch.BaseDelegatingView
import com.bigscreen.defineword.arch.BasePresenter
import com.bigscreen.defineword.arch.BaseView
import com.bigscreen.defineword.arch.UserAction

class HomeContract {

    interface DelegatingView : BaseDelegatingView<ViewModel>

    interface View : BaseView<ViewModel>

    abstract class Presenter : BasePresenter<ViewModel, View>() {
        abstract fun onUserAction(action: UserAction)
    }

    sealed class ViewModel {
        data class HomeViewState(
                val wordCategory: String,
                val wordDefinition: String
        ) : ViewModel()
    }
}

sealed class HomeUserAction : UserAction {
    data class ActionSearch(val keyword: String) : HomeUserAction()
}