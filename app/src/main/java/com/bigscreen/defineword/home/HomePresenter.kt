package com.bigscreen.defineword.home

import com.bigscreen.defineword.arch.UserAction
import com.bigscreen.defineword.workflows.WordEntriesWorkflow
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class HomePresenter @Inject constructor(
        private val workflow: WordEntriesWorkflow
) : HomeContract.Presenter() {

    private var findDisposable: Disposable? = null
    private var showDisposable: Disposable? = null
    private var errorDisposable: Disposable? = null

    override fun onAttach() {
        observe { view().actions().subscribe(::onUserAction) }
    }

    override fun onUserAction(action: UserAction) {
        when (action) {
            is HomeUserAction.ActionSearch -> findWordEntry(action.keyword)
        }
    }

    private fun findWordEntry(keyword: String) {
        findDisposable?.dispose()
        findDisposable = workflow.getWordEntry(keyword)
                .subscribe(
                        {
                            showDisposable?.dispose()
                            showDisposable = view().showData(it).subscribe(::onUserAction)
                        },
                        {
                            errorDisposable?.dispose()
                            errorDisposable = view().showError(it).subscribe(::onUserAction)
                        }
                )
    }

    override fun onDetach() {
        findDisposable?.dispose()
        showDisposable?.dispose()
        errorDisposable?.dispose()
    }

}