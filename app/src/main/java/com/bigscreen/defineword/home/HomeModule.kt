package com.bigscreen.defineword.home

import dagger.Module
import dagger.Provides
import dagger.Subcomponent

@Subcomponent(modules = [(HomeModule::class)])
interface HomeComponent {
    fun inject(homeActivityDelegate: HomeActivityDelegate)
}

@Module
class HomeModule {

    @Provides
    fun providesPresenter(presenter: HomePresenter): HomeContract.Presenter = presenter
}
