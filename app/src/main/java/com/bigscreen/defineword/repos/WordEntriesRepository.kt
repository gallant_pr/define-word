package com.bigscreen.defineword.repos

import com.bigscreen.defineword.ext.configured
import com.bigscreen.defineword.network.ApiService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class WordEntriesRepository @Inject constructor(private val apiService: ApiService) {

    fun getWordEntries(keyword: String) = apiService.getWordEntries(keyword).configured()
}