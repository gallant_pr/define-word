package com.bigscreen.defineword.ext

import android.widget.EditText

fun EditText.getString(): String = text.toString()