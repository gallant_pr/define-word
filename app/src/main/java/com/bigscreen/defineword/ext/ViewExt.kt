package com.bigscreen.defineword.ext

import android.view.View
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

fun View.rxClick(): Observable<Any> = RxView.clicks(this)
        .debounce(200, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())