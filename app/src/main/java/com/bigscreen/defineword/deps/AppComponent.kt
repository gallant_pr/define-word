package com.bigscreen.defineword.deps

import com.bigscreen.defineword.home.HomeComponent
import com.bigscreen.defineword.home.HomeModule
import com.bigscreen.defineword.network.NetworkModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    NetworkModule::class
])
interface AppComponent {

    fun plus(module: HomeModule): HomeComponent
}