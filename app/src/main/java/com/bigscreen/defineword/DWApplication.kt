package com.bigscreen.defineword

import android.app.Application
import com.bigscreen.defineword.deps.AppComponent
import com.bigscreen.defineword.deps.AppModule
import com.bigscreen.defineword.deps.DaggerAppComponent
import com.bigscreen.defineword.network.NetworkModule
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import kotlin.properties.Delegates

class DWApplication : Application() {

    var appComponent: AppComponent by Delegates.notNull()

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .networkModule(NetworkModule(BuildConfig.BASE_URL))
                .build()
        Fabric.with(this, Crashlytics())
    }
}