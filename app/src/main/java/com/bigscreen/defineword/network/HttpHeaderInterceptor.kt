package com.bigscreen.defineword.network

import okhttp3.Interceptor
import okhttp3.Response

class HttpHeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request()
                .newBuilder()
                .addHeader("app_id", "8cdf7648")
                .addHeader("app_key", "a1841de47089ede5c332116b65428ee0")
                .build()
        return chain.proceed(newRequest)
    }

}