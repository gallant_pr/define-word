package com.bigscreen.defineword.network.model

import com.google.gson.annotations.SerializedName

data class WordEntriesResponse(
        @SerializedName("results") val results: List<EntryResult> = arrayListOf()
)

data class EntryResult(
        @SerializedName("lexicalEntries") val lexicalEntries: List<LexicalEntry> = arrayListOf()
)

data class LexicalEntry(
        @SerializedName("entries") val entries: List<Entry> = arrayListOf(),
        @SerializedName("lexicalCategory") val lexicalCategory: String = ""
)

data class Entry(@SerializedName("senses") val senses: List<Sense> = arrayListOf())

data class Sense(@SerializedName("definitions") val definitions: List<String> = arrayListOf())

data class WordEntry(var category: String = "", var definition: String = "") {

    companion object {
        fun fromResponse(response: WordEntriesResponse) = WordEntry().apply {
            category = response.results.first().lexicalEntries.first().entries.first()
                    .senses.first().definitions.first()
            definition = response.results.first().lexicalEntries.first().lexicalCategory
        }
    }
}