package com.bigscreen.defineword.network

import com.bigscreen.defineword.network.model.WordEntriesResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("entries/en/{keyword}")
    fun getWordEntries(@Path("keyword") keyword: String): Observable<WordEntriesResponse>
}