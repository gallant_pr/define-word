package com.bigscreen.defineword.stores

import com.bigscreen.defineword.home.HomeContract
import com.bigscreen.defineword.network.model.WordEntriesResponse
import javax.inject.Inject

class WordEntriesStore @Inject constructor() {

    data class State(
            val category: String = "",
            val definition: String = ""
    )

    private var state = State()

    fun update(response: WordEntriesResponse): HomeContract.ViewModel {
        state = state.copy(
                category = response.results.first().lexicalEntries.first().lexicalCategory,
                definition = response.results.first().lexicalEntries.first().entries.first()
                        .senses.first().definitions.first()
        )
        return deriveViewState(state)
    }

    private fun deriveViewState(state: State): HomeContract.ViewModel {
        return HomeContract.ViewModel.HomeViewState(
                state.category,
                state.definition
        )
    }

}