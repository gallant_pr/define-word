package com.bigscreen.defineword.workflows

import com.bigscreen.defineword.home.HomeContract
import com.bigscreen.defineword.repos.WordEntriesRepository
import com.bigscreen.defineword.stores.WordEntriesStore
import io.reactivex.Observable
import javax.inject.Inject

class WordEntriesWorkflow @Inject constructor(
        private val repository: WordEntriesRepository,
        private val store: WordEntriesStore
) {

    fun getWordEntry(keyword: String): Observable<HomeContract.ViewModel> =
            repository
                    .getWordEntries(keyword)
                    .map { store.update(it) }
}