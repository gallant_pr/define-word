package com.bigscreen.defineword

import com.google.gson.Gson
import okhttp3.MediaType
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.ResponseBody
import okio.BufferedSource
import retrofit2.HttpException
import retrofit2.Response
import java.io.InputStreamReader

fun <T> getObjectFromResources(clazz: Class<T>, fileName: String): T {
    val inputStreamReader = InputStreamReader(clazz.classLoader.getResourceAsStream(fileName))
    return Gson().fromJson(inputStreamReader, clazz)
}

fun mockErrorResponse(errorCode: Int, message: String): HttpException {
    return retrofit2.HttpException(
            getErrorResponse(errorCode, message, object : ResponseBody() {
                override fun contentType(): MediaType? = null
                override fun contentLength(): Long = 0
                override fun source(): BufferedSource? = null
            })
    )
}

private fun getErrorResponse(statusCode: Int, message: String, responseBody: ResponseBody): Response<ResponseBody> {
    return Response.error<ResponseBody>(
            responseBody, okhttp3.Response.Builder()
            .code(statusCode)
            .protocol(Protocol.HTTP_1_1)
            .request(Request.Builder().url("http://localhost/").build())
            .message(message)
            .build())
}