package com.bigscreen.defineword.home

import com.bigscreen.defineword.mockErrorResponse
import com.bigscreen.defineword.workflows.WordEntriesWorkflow
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class HomePresenterTest {

    @Mock lateinit var workflow: WordEntriesWorkflow
    @Mock lateinit var view: HomeContract.View

    private lateinit var presenter: HomePresenter

    @Before
    fun `set up`() {
        MockitoAnnotations.initMocks(this)
        presenter = HomePresenter(workflow)
        whenever(view.actions()).thenReturn(Observable.never())
        presenter.attach(view)
    }

    @Test
    fun `find word entry should success to show data`() {
        whenever(view.showData(any())).thenReturn(Observable.never())
        whenever(workflow.getWordEntry("code")).thenReturn(Observable.just(viewModel()))
        presenter.onUserAction(HomeUserAction.ActionSearch("code"))
        verify(view).showData(viewModel())
    }

    @Test
    fun `find word entry should failed when error`() {
        val error = mockErrorResponse(404, "Not Found!")
        whenever(view.showError(any())).thenReturn(Observable.never())
        whenever(workflow.getWordEntry("code")).thenReturn(Observable.error(error))
        presenter.onUserAction(HomeUserAction.ActionSearch("code"))
        verify(view).showError(error)
    }

    private fun viewModel(): HomeContract.ViewModel = HomeContract.ViewModel.HomeViewState(
            wordCategory = "Noun",
            wordDefinition = "a system of words, letters, figures, or symbols used to " +
                    "represent others, especially for the purposes of secrecy"
    )
}