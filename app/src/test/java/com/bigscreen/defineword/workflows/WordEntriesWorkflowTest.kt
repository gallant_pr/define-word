package com.bigscreen.defineword.workflows

import com.bigscreen.defineword.home.HomeContract
import com.bigscreen.defineword.network.model.WordEntriesResponse
import com.bigscreen.defineword.repos.WordEntriesRepository
import com.bigscreen.defineword.stores.WordEntriesStore
import com.bigscreen.defineword.getObjectFromResources
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.io.IOException

class WordEntriesWorkflowTest {

    @Mock lateinit var repository: WordEntriesRepository
    @Mock lateinit var store: WordEntriesStore

    private lateinit var workflow: WordEntriesWorkflow

    @Before
    fun `set up`() {
        MockitoAnnotations.initMocks(this)
        workflow = WordEntriesWorkflow(repository, store)
    }

    @Test
    fun `get word entries should returns correct view model`() {
        val response = getObjectFromResources(
                WordEntriesResponse::class.java,
                "response_word_entries.json")
        val testObserver = TestObserver<HomeContract.ViewModel>()
        whenever(repository.getWordEntries("code")).thenReturn(Observable.just(response))
        whenever(store.update(response)).thenReturn(viewModel())
        workflow.getWordEntry("code").subscribe(testObserver)

        testObserver.assertComplete()
        testObserver.assertNoErrors()
        testObserver.assertValue(viewModel())
    }

    @Test
    fun `get word entries should throw throwable`() {
        val error = IOException()
        val testObserver = TestObserver<HomeContract.ViewModel>()
        whenever(repository.getWordEntries("code")).thenReturn(Observable.error(error))
        workflow.getWordEntry("code").subscribe(testObserver)

        testObserver.assertNotComplete()
        testObserver.assertNoValues()
        testObserver.assertError(error)
    }

    private fun viewModel(): HomeContract.ViewModel = HomeContract.ViewModel.HomeViewState(
                wordCategory = "Noun",
                wordDefinition = "a system of words, letters, figures, or symbols used to " +
                        "represent others, especially for the purposes of secrecy"
        )
}