package com.bigscreen.defineword.stores

import com.bigscreen.defineword.home.HomeContract
import com.bigscreen.defineword.network.model.WordEntriesResponse
import com.bigscreen.defineword.getObjectFromResources
import org.junit.Assert.assertEquals
import org.junit.Test

class WordEntriesStoreTest {

    @Test
    fun `update store should derive correct data`() {
        val store = WordEntriesStore()
        val response = getObjectFromResources(
                WordEntriesResponse::class.java,
                "response_word_entries.json")
        assertEquals(viewModel(), store.update(response))
    }

    private fun viewModel(): HomeContract.ViewModel = HomeContract.ViewModel.HomeViewState(
            wordCategory = "Noun",
            wordDefinition = "a system of words, letters, figures, or symbols used to " +
                    "represent others, especially for the purposes of secrecy"
    )
}