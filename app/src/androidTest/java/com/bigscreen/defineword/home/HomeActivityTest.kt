package com.bigscreen.defineword.home

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.closeSoftKeyboard
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.test.rule.ActivityTestRule
import com.bigscreen.defineword.R
import com.bigscreen.defineword.helpers.InstrumentationEnvironmentRule
import com.bigscreen.defineword.helpers.MockWebServerRule
import com.bigscreen.defineword.helpers.getStringFromFile
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test

class HomeActivityTest {

    @get:Rule
    val instrumentationEnvironmentRule = InstrumentationEnvironmentRule()

    @get:Rule
    val mockWebServerRule = MockWebServerRule()

    @get:Rule
    val activityRule = ActivityTestRule(HomeActivity::class.java, true, false)

    @Test
    fun searchWordShouldShowWordsDefinition() {
        mockWebServerRule.mock(200, getStringFromFile("response_word_entries.json"))
        activityRule.launchActivity(getHomeIntent())
        onView(withId(R.id.inputWord)).perform(typeText("code"), closeSoftKeyboard())
        onView(withId(R.id.buttonSearch)).perform(click())
        Thread.sleep(500)
        onView(withId(R.id.textWordCategory)).check(matches(allOf(isDisplayed(), withText("Noun"))))
        onView(withId(R.id.textWordDefinition)).check(matches(allOf(isDisplayed(), withText(
                "a system of words, letters, figures, or symbols used to " +
                "represent others, especially for the purposes of secrecy"
        ))))
    }

    @Test
    fun searchWordShouldShowError() {
        mockWebServerRule.mock(404, "Something went wrong.")
        activityRule.launchActivity(getHomeIntent())
        onView(withId(R.id.inputWord)).perform(typeText("code"), closeSoftKeyboard())
        onView(withId(R.id.buttonSearch)).perform(click())
        Thread.sleep(500)
        onView(withId(R.id.textWordCategory)).check(matches(allOf(isDisplayed(), withText("Ooops!"))))
        onView(withId(R.id.textWordDefinition)).check(matches(allOf(isDisplayed(), withText(
                "We can not define your word, try another one."
        ))))
    }

    private fun getHomeIntent() = Intent(InstrumentationRegistry.getTargetContext(), HomeActivity::class.java)
}