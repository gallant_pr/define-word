package com.bigscreen.defineword.helpers

import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import com.bigscreen.defineword.network.NetworkModule
import android.support.test.InstrumentationRegistry
import com.bigscreen.defineword.DWApplication
import com.bigscreen.defineword.deps.AppModule
import com.bigscreen.defineword.deps.DaggerAppComponent
import com.bigscreen.defineword.helpers.TestConstants.BASE_URL
import com.bigscreen.defineword.helpers.TestConstants.PORT

class InstrumentationEnvironmentRule : TestRule {

    override fun apply(base: Statement?, description: Description?): Statement =
            InstrumentationEnvironmentStatement(base)

}

private class InstrumentationEnvironmentStatement(
        private val baseStatement: Statement?
) : Statement() {

    @Throws(Throwable::class)
    override fun evaluate() {
        val application = InstrumentationRegistry.getTargetContext().applicationContext as DWApplication
        application.appComponent =
                DaggerAppComponent
                        .builder()
                        .appModule(AppModule(InstrumentationRegistry.getTargetContext()))
                        .networkModule(NetworkModule("$BASE_URL:$PORT"))
                        .build()
        baseStatement?.evaluate()
    }

}