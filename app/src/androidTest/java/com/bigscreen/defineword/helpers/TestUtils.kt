package com.bigscreen.defineword.helpers

import android.support.test.InstrumentationRegistry
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream

fun getStringFromFile(fileName: String): String {
    return try {
        val fileInputStream = InstrumentationRegistry.getContext().assets.open(fileName)
        getStringFromInputStream(fileInputStream)
    } catch (e: IOException) {
        e.printStackTrace()
        ""
    }
}

private fun getStringFromInputStream(inputStream: InputStream) =
        inputStream.bufferedReader().use(BufferedReader::readText)