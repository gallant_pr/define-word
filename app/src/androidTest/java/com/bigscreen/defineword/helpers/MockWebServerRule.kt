package com.bigscreen.defineword.helpers

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

class MockWebServerRule : TestRule {

    private val mockWebServer = MockWebServer()

    override fun apply(base: Statement?, description: Description?): Statement =
            MockHTTPServerStatement(base, mockWebServer)

    fun mock(responseCode: Int, response: String) {
        mockWebServer.enqueue(
                MockResponse()
                        .setResponseCode(responseCode)
                        .setBody(response)
        )
    }

}

private class MockHTTPServerStatement(
        private val baseStatement: Statement?,
        private val mockWebServer: MockWebServer
) : Statement() {

    @Throws(Throwable::class)
    override fun evaluate() {
        mockWebServer.start(TestConstants.PORT)
        try {
            baseStatement?.evaluate()
        } finally {
            mockWebServer.shutdown()
        }
    }
}